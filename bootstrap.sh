#!/usr/bin/env bash

# Install latest macOS software updates
# sudo softwareupdate -i -a

# Install command line tools
# xcode-select --install

git clone https://gurpreet-juttla@bitbucket.org/gurpreet-juttla/dotfiles.git
cd ./dotfiles
export DOTFILES_DIR="$(pwd)" 

source $DOTFILES_DIR/install.sh
